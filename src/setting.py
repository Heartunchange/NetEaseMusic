# -*- coding:utf8 -*-

"""
common settings

FUTURE:  read setting from a json file
"""


"""
path configuration
"""
ICON_PATH = '../icons/'
CACHE_PATH = '../cache/'
DATA_PATH = '../DATA/'


"""
mode configuration
"""
DEBUG = True   # 1 for debug
PRODUCTION = False  # 0 for Production Environment
MODE = DEBUG


"""
theme configuration
"""
QSS_PATH = 'themes/default.qss'